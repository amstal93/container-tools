SHELL = /bin/bash
DOCKER_IMAGE_TAG = latest
DOCKER_IMAGE_NAME = "registry.gitlab.com/tkn-3d-printing/container-tools"

help:
	@cat README.rst
	@echo
	@echo "Current settings"
	@echo "----------------"
	@echo ""
	@echo "DOCKER_IMAGE_NAME = ${DOCKER_IMAGE_NAME}"
	@echo "DOCKER_IMAGE_TAG = ${DOCKER_IMAGE_TAG}"
	@echo ""

build: Dockerfile check_tag check_name
	docker build -t "${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG}" .

check_tag:
	@[ -n "${DOCKER_IMAGE_TAG}" ] || { echo "Missing DOCKER_IMAGE_TAG, please specify -e DOCKER_IMAGE_TAG=XX.Y" 1>&2; exit 1; }

check_name:
	@[ -n "${DOCKER_IMAGE_NAME}" ] || { echo "Missing DOCKER_IMAGE_NAME, please specify -e DOCKER_IMAGE_NAME=name" 1>&2; exit 1; }

clean: check_exists
	docker rmi "${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG}"

check_exists:
	@[ `docker images ${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG} | head -n-1 | wc -l` -eq 1 ] || { echo "Missing DOCKER_IMAGE_NAME, please run make build" 1>&2; exit 1; }

clean_dangling:
	docker rmi `docker images --filter dangling=true --format='{{.ID}}'`

start_openscad: check_exists
	docker run -ti --rm \
		--volume="/home/$$USER:/home/$$USER" \
		--volume="/etc/group:/etc/group:ro" \
		--volume="/etc/passwd:/etc/passwd:ro" \
		--volume="/etc/shadow:/etc/shadow:ro" \
		--volume="/etc/sudoers.d:/etc/sudoers.d:ro" \
		--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
		--volume /dev/shm:/dev/shm \
		--env="DISPLAY" \
		--workdir="/home/$$USER" \
		--user="$$UID:$$(id -g)" \
		${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG} \
		/usr/bin/openscad

start_slic3r: check_exists
	docker run -ti --rm \
		--volume="/home/$$USER:/home/$$USER" \
		--volume="/etc/group:/etc/group:ro" \
		--volume="/etc/passwd:/etc/passwd:ro" \
		--volume="/etc/shadow:/etc/shadow:ro" \
		--volume="/etc/sudoers.d:/etc/sudoers.d:ro" \
		--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
		--volume /dev/shm:/dev/shm \
		--env="DISPLAY" \
		--workdir="/home/$$USER" \
		--user="$$UID:$$(id -g)" \
		${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG} \
		/usr/bin/slic3r

